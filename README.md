# mysqlapp

Tugas Akhir Sanbercode Python Microservice

Pembuatan Mysql App dengan Rest API, Flask, dan JWT untuk database perpustakaan
Didalam mysqlapp terdapat Controllers, Models, Routers untuk customers dan borrows

Rest api untuk menghubungkan aplikasi satu dengan yang lain (menyambungkan customer dengan borrow dan database lain)
 Flask di Python untuk membuat REST API
JWT digunakan sebagai authorisasi user yang masuk beeserta email bahwa user telah login dan logout atau belum.
