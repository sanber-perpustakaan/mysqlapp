from app import app
from app.controllers import customers_controller
from flask import Blueprint, request

customer_blueprint = Blueprint("borrows_router", __name__)


@app.route("/borrows", methods="GET")
def showBorrow():
    return borrow_controller.sh()


@app.route("/borrows/insert", methods="POST")
def insertBorrow():
    return borrow_controller.insert(**params)


@app.route("/borrows/status", methods="POST")
def updateStatus():
    params = request.json
    return borrow_controller.changeStatus(**params)
