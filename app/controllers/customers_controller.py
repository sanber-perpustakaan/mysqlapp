from app.models.customers_model import database
from flask import jsonify, request
from flask_jwt_extended import *

mysqdb = database()


def shows():
    dbresult = mysqldb.showUsers()
    result = []
    for items in dbresult:
        user = {
            "id": items[0]
            "username": items[1]
            "firstname": items[2]
            "lastname": items[3]
            "email": items[4]
        }
        result.append(user)
    return jsonify(result)


def show(**params):
    dbresult = mysqldb.showUsersById(**params)
    result = []
    for items in dbresult:
        user = {
            "id": dbresult[0]
            "username": dbresult[1]
            "firstname": dbresult[2]
            "lastname": dbresult[3]
            "email": dbresult[4]
        }
        result.append(user)
    return jsonify(result)


def delete(**params):
    mysqdb.deleteUserById(**params)
    mysqdb.dataCommit()
    return jsonify({"message": "success"})


def update(**params):
    mysqdb.updateUserById(**params)
    mysqdb.dataCommit()
    return jsonify({"message": "success"})


def token(**params):
    dbresult = mysqdb.showUserByEmail(**params)
    if dbresult is not None:
        user = {
            "username": dbresult[1],
            "email": dbresult[4]
        }
        expire = datetime.timedelta(days=1)
        access_token = create_access_token(
            user, fresh=True, expires_delta=expires)

        data = {
            "data": user,
            "token_access": access_token
        }
    else:
        data = {
            "message": "email tidak terdaftar"
        }
    return jsonify(data)
