from app.models.borrows_model import database
from flask import jsonify, request
from flask_jwt_extended import *
import json
import datetime
import requests

mysqldb = database()


@jwt_required()
def show():
    params = get_jwt_identity()
    dbresult = mysql.showBorrowByEmail(**params)
    result = []
    if dbresult is not None:
        for item in dbresult:
            if = json.dumps({"id": items{4}})
            bookdetail = getBookById(id)
            user = {
                "username": items[0],
                "borrowid": items[1],
                "borrowdate": items[2],
                "bookid": items[4],
                "bookname": items[5],
                "author": bookdetail["pengarang"]
                "releaseyear": bookdetail["tahunterbit"]
                "genre": bookdetail["genre"]

            }
            result.append(user)
    else:
        result = dbresult
    return jsonify(result)


@jwt_required()
def insert():
    token = get_jwt_identity()
    userid = cust_db.showUserByEmail(**token)[0]
    borrowdate = datetime.datetime.now().isoformat()
    id = json.dumps({"id": params["bookid"]})
    bookname = getBookById(id)["nama"]
    params.update(
        {
            "userid": userid,
            "borrowdate": borrowdate,
            "bookname": bookname,
            "isactive": 1
        }
    )
    mysqldb.insertBorrow(**params)
    mysqldb.dataCommit()
    return jsonify({"message": "Success"})


@jwt_required()
def changeStatus(**params):
    mysqldb.updateBorrow(**params)
    mysqldb.dataCommit()
    return jsonify("message": "Success")


def getBookById(data):
    book_data = requests.get(url="http://localhost:8000/bookbyid", data=data)
    return book_data.json()
